<?php

/**
 * @file
 * Pane mover pane.
 */

$plugin = array(
  'title' => t('Pane Mover wormhole'),
  'single' => TRUE,
  'content_types' => array('panemover_pane'),
  'defaults' => array('key' => ''), // This seems not to prepopulate $conf.
  'edit form' => 'panemover_pane_edit_form',
  'admin title' => 'panemover_pane_admin_title',
  'render callback' => 'panemover_pane_render',
  'category' => array(t('Pane Mover'), -9),
  'render last' => TRUE,
);

function panemover_pane_render($subtype, $conf, $args, $context) {
  $key = $conf['key'];
  $content =& panemover_get_reference($key);

  $block = new stdClass();
  $block->content =& $content;
  return $block;
}

function panemover_pane_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#size' => 50,
    '#description' => t('The key to get the pane content from.'),
    '#default_value' => !empty($conf['key']) ? $conf['key'] : '',
    '#required' => TRUE,
  );
  return $form;
}

function panemover_pane_edit_form_submit($form, &$form_state) {
  $form_state['conf']['key'] = $form_state['values']['key'];
}

function panemover_pane_admin_title($subtype, $conf, $context = NULL) {
  $output = t('Panemover: %key', array('%key' => $conf['key']));
  return $output;
}
