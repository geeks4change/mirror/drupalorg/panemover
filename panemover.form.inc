<?php
/**
 * @file panemover.form.inc
 */

/**
 * Form callback.
 */
function panemover_blocks_form($form, &$form_state) {
  $form['panemover_blocks'] = array(
    '#type' => 'item',
    '#title' => t('Pane Mover overview'),
    '#description' => t(
      'Every panels pane content that you apply the Pane Mover Black Hole style, will not bedisplayed, but saved under its configured key. '
      . 'You can get it back a) via the Pane Mover Worm Hole panel pane under the same key, or b) via pre-configured blocks.'
    )
  );
  $form['panemover_block_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Pane Mover blocks'),
    '#description' => t('Configure the number of pane mover blocks here.'),
    '#default_value' => variable_get('panemover_block_count', 0),
    '#element_validate' => array('panemover_validate_integer_nonnegative'),
  );

  $form = system_settings_form($form);
  // CLear the blocks table so new blocks are shown.
  $form['#submit'][] = 'panemover_blocks_form_submit';
  return $form;
}

/**
 * Element validate callback.
 *
 * Stolen from element_validate_integer_positive().
 */
function panemover_validate_integer_nonnegative($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
}

function panemover_blocks_form_submit($form, &$form_state) {
  $bins = block_flush_caches();
  foreach ($bins as $bin) {
    cache_clear_all('*', $bin, TRUE);
  }
}
